import json
import datetime

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
import yfinance as yf

conf = SparkConf().setAppName("My PySpark App").setMaster(
    "spark://spark-master:7077")  # опять-таки должно совпадать с contain
sc = SparkContext(conf=conf)
# Start the SparkSession
spark = SparkSession.builder \
    .config(conf=conf) \
    .getOrCreate()

# Тут тело скрипта, что вообще требуется выполнить/посчитать и тд
symbol = "USDRUB=X"
# Задаем дату начала и конца периода, за который хотим получить данные (за прошедший день)
end_date = datetime.datetime.now()
start_date = end_date - datetime.timedelta(days=1)
#  Загружаем данные
data = yf.download(symbol, start=start_date, end=end_date)
final_vals = {'dollar_rate': data['Adj Close'].iloc[-1], 'min_price': data['Low'].iloc[-1],
              'max_price': data['High'].iloc[-1]}
res_string = f"Курс доллара к рублю за прошедший день: {final_vals['dollar_rate']} RUB\n"
res_string += f"Наименьшая цена: {final_vals['min_price']} RUB\n"
res_string += f"Наибольшая цена: {final_vals['max_price']} RUB\n"
with open("res.txt", "w") as text_file:
    print(res_string, file=text_file)

# Stop the SparkSession
spark.stop()
