import datetime
import json
import textwrap

import pendulum
import yfinance as yf
from airflow.models.dag import DAG
from airflow.operators.python import PythonOperator

with DAG(
        "Stock_market_dag",
        # [START default_args]
        # These args will get passed on to each operator
        # You can override them on a per-task basis during operator initialization
        default_args={"retries": 2},
        # [END default_args]
        description="DAG for collecting info for stock market",
        schedule=None,
        start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
        catchup=False,
) as dag:
    dag.doc_md = __doc__


    def collect(**kwargs):
        ti = kwargs["ti"]
        symbol = "USDRUB=X"
        # Задаем дату начала и конца периода, за который хотим получить данные (за прошедший день)
        end_date = datetime.datetime.now()
        start_date = end_date - datetime.timedelta(days=1)
        #  Загружаем данные
        data = yf.download(symbol, start=start_date, end=end_date)
        ti.xcom_push("stock_data", data)


    def process(**kwargs):
        ti = kwargs["ti"]
        data = ti.xcom_pull(task_ids="collect", key="stock_data")
        final_vals = {'dollar_rate': data['Adj Close'].iloc[-1], 'min_price': data['Low'].iloc[-1],
                      'max_price': data['High'].iloc[-1]}
        passed_vals = json.dumps(final_vals)
        ti.xcom_push("processed_data", passed_vals)


    def load(**kwargs):
        ti = kwargs["ti"]
        vals = ti.xcom_pull(task_ids="process", key="processed_data")
        final_vals = json.loads(vals)

        res_string = f"Курс доллара к рублю за прошедший день: {final_vals['dollar_rate']} RUB\n"
        res_string += f"Наименьшая цена: {final_vals['min_price']} RUB\n"
        res_string += f"Наибольшая цена: {final_vals['max_price']} RUB\n"
        with open("res.txt", "w") as text_file:
            print(res_string, file=text_file)


    collect_task = PythonOperator(
        task_id="collect",
        python_callable=collect,
    )

    collect_task.doc_md = textwrap.dedent(
        """\
    ### Задача сбора данных
    Данная задача производит сбор данных о бирже.
    """
    )

    process_task = PythonOperator(
        task_id="process",
        python_callable=process,
    )

    process_task.doc_md = textwrap.dedent(
        """\
    ### Задача обработки
    Данная задача обрабатывает собранную информацию для подготовки ее к отображению.
    """
    )

    load_task = PythonOperator(
        task_id="load",
        python_callable=load,
    )

    load_task.doc_md = textwrap.dedent(
        """\
    ### Задача загрузки
    Задача, которая сохраняет результат прошлых задач
    """
    )

    collect_task >> process_task >> load_task
