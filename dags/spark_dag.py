import datetime
import json
import textwrap

import pendulum
import yfinance as yf
from airflow.models.dag import DAG
from airflow.operators.python import PythonOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator

dag = DAG(
    "Stock_market_spark_dag",
    # [START default_args]
    # These args will get passed on to each operator
    # You can override them on a per-task basis during operator initialization
    default_args={"retries": 2},
    # [END default_args]
    description="SPARK DAG for collecting info for stock market",
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
)

spark_job = SparkSubmitOperator(task_id='my_spark_job',  # имя любое
                                application=f'/opt/airflow/spark/test_spark.py',
                                # путь до скрипта, который надо прогнать
                                name='my_spark_job',  # имя любое
                                conn_id='spark',
                                # должно совпадать с именем, заданным через админку при создании подклю
                                dag=dag)
